# -*- coding: utf-8 -*-
"""
Created on Mon Jan 15 10:43:13 2018

@author: enovi
"""

#Todo: figure out how to load the XBRL data from EDGAR into the datapoints
#Todo: print to a text file or word document for each company

import random

#Make stock market articles!
def main():
    company_name     = 0
    profit_thisq     = 0
    profit_yagoq     = 0
    revenue_thisq    = 0
    revenue_yagoq    = 0
    number           = 0
    adjuster         = 1
    earnings_growth  = 0
    revenue_growth   = 0
    hq_city          = ''
    employee_num     = 0
    sector_name      = ''
    founded_date     = 0
    
    #Input the data
    #Enter data fields manually, for now. Add XBRL parsing code later.
    print('Welcome to Stock Market Article Generator!')
    company_name    = input('Enter the company name: ')
    profit_thisq    = int(input('Enter net income for this quarter: '))
    profit_yagoq    = int(input('Enter net income for the same quarter a year ago: '))
    if profit_thisq >= profit_yagoq:
        earnings_growth = ((profit_thisq / profit_yagoq) - 1) * 100
    elif profit_yagoq > profit_thisq:    
        earnings_growth = ((profit_thisq - profit_yagoq) / profit_yagoq) * 100
    earnings_growth = int(earnings_growth)    
    revenue_thisq   = int(input('Enter revenue for this quarter: '))
    revenue_yagoq   = int(input('Enter revenue for the same quarter a year ago: '))
    if revenue_thisq >= revenue_yagoq:
        revenue_growth  = ((revenue_thisq / revenue_yagoq) - 1) * 100
    elif revenue_yagoq > revenue_thisq:    
        revenue_growth = ((revenue_thisq - revenue_yagoq) / revenue_yagoq) * 100
    revenue_growth = int(revenue_growth)    
    hq_city         = input('Enter city where corporate headquarters is located: ')
    employee_num    = int(input('Enter number of employees: '))
    sector_name     = input('Enter sector name: ') 
    founded_date    = int(input('Enter founding date: '))
    
    #Write the article
    earnings_release = earnings_release_phrase()
    number = earnings_growth
    adj_1 = adjective_1(number)
    v_1 = verb_1(number)
    adjuster = adjust(number)
    print('________________')
    print('{} {} today.'.format(company_name, earnings_release) +
    ' It was a {} quarter.'.format(adj_1) +
    ' Earnings {} {} percent year-over-year.'.format(v_1, earnings_growth * adjuster))
    number = revenue_growth
    v_1 = verb_1(number)
    adjuster = adjust(number)
    print('Revenue {} {} percent year-over-year.'.format(v_1, revenue_growth * adjuster))
    print('{} is located in {}'.format(company_name, hq_city) +
    ' and has {} employees.'.format(employee_num))
    adj_2 = adjective_2()
    adj_3 = adjective_3()
    print('{} {} in the {} sector.'.format(company_name, adj_2, sector_name) +
    ' The company was {} in {}.'.format(adj_3, founded_date))
    
# Removes the minus sign so the article doesn't say a stock fell by -10 percent    
def adjust(number):
    if number < 0:
        adjuster = -1
    else:
        adjuster = 1
    return adjuster    
    
def earnings_release_phrase():
    choice = 0
    value = []
    value = ['released earnings','issued its quarterly report','provided an update', 'posted its 10-Q']
    choice = random.randint(0,len(value) - 1)
    return value[choice]
      
def adjective_1(number):
    choice = 0
    value = []
    if number > 0:
        value = ['strong','hot','high-quality','spectacular','great','nice','powerful']
    elif number == 0:
       value = ['flat']
    elif number < 0:
        value = ['weak','disappointing','bad','tough']
    choice = random.randint(0,len(value) - 1)
    return value[choice]

def adjective_2():
    choice = 0
    value = []
    value = ['operates','does business','competes','conducts business','sells its wares']
    choice = random.randint(0,len(value) - 1)
    return value[choice]

def adjective_3():
    choice = 0
    value = []
    value = ['founded','launched','established','created','started']
    choice = random.randint(0,len(value) - 1)
    return value[choice]

def verb_1(number):
    choice = 0
    value = 0
    if number > 0:
        value = ['increased','soared','skyrocketed','climbed','spiked']
    elif number == 0:
        value = ['was flat','was unchanged','showed little movement']
    elif number < 0:    
        value = ['fell','dropped','crashed','declined','tanked']
    choice = random.randint(0,len(value) - 1)
    return value[choice]    
    
main()